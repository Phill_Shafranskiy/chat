import React, { FC } from 'react';
import './App.css';
import Chat from './Chat'

const App: FC = () => {
  return (
    <div className="App">
      <Chat url="https://edikdolynskyi.github.io/react_sources/messages.json" />
    </div>
  );
};

export default App;