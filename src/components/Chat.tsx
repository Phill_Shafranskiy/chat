import React, { FC, useEffect, useState } from 'react';
import Header from './Header/Header';
import MessageList from './MessageList/MessageList';
import MessageInput from './MessageInput/MessageInput';
import { MessageItem } from '../types/types';
import Preloader from './Preloader/Preloader';

interface UrlProps {
  url: string;
}

const Chat: FC<UrlProps> = ({ url }) => {
  const [loading, setLoading] = useState(true)
  const [data, setData] = useState([])
  const [ownMessage, setOwnMessage] = useState<MessageItem>()


  useEffect(() => {
    fetch(url)
      .then(res => res.json())
      .then(setData)
      .then(() => setTimeout(() => setLoading(false), 2000))
  }, [])

  const messages: MessageItem[] = data;
  const uniqueParticipants: Array<string> = messages.map(item => item.user)
    .filter((value, index, self) => self.indexOf(value) === index)
  const lastMessage = messages.pop() ? messages.pop()!.createdAt : '';

  if (ownMessage) messages.push(ownMessage)

  console.log(ownMessage);


  if (loading) {
    return <Preloader />
  }
  console.log(messages);


  return (
    <div>
      <Header messagesAll={data.length} lastMessage={lastMessage} unique={uniqueParticipants.length} />
      <MessageList messages={messages} />
      <MessageInput setOwnMessage={setOwnMessage} />
    </div>
  );
};

export default Chat;