import React, { FC } from 'react';
import './Header.css';

interface ChatInfoProps {
  messagesAll: number;
  lastMessage: string;
  unique: number;
}

const Header: FC<ChatInfoProps> = ({ messagesAll, lastMessage, unique }) => {
  const localDateTime = new Date(lastMessage).toLocaleDateString() + " " + lastMessage.slice(11, 19);

  return (
    <div className='header'>
      <h1 className='header-title'>My chat</h1>
      <div className="header-users-count">{unique + 1 + ' participants'}</div>
      <div className="header-messages-count">{messagesAll + 1 + ' messages'}</div>
      <div className="header-last-message-date">{'last message at ' + localDateTime}</div>
    </div>
  );
};

export default Header;