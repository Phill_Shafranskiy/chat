import React, { FC, useState, Dispatch } from 'react';
import './MessageInput.css';

type Props = {
  setOwnMessage: React.Dispatch<React.SetStateAction<any>>;
};


const MessageInput: FC<Props> = ({ setOwnMessage }) => {
  const [value, setValue] = useState<string>('');

  const changeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
    setValue(e.target.value);
  }

  const clickHandler = (e: React.MouseEvent<HTMLButtonElement>) => {
    setOwnMessage({
      id: '',
      userId: '1',
      avatar: '',
      user: 'Phill',
      text: value,
      createdAt: new Date().toISOString(),
      editedAt: ''
    })
  }

  return (
    <div className='message-input'>
      <input type="text" className='message-input-text' placeholder='Message...' onChange={changeHandler} />
      <button className='message-input-button' onClick={clickHandler}>Send</button>
    </div>
  );
};

export default MessageInput;