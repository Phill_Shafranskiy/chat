import React, { FC } from 'react';
import './Message.css';
import { MessageItem } from '../../../types/types';

interface MessageItemProps {
  message: MessageItem;
}

const Message: FC<MessageItemProps> = ({ message }) => {
  return (
    <div className='message'>
      <img src={message.avatar} alt="avatar" className="message-user-avatar" />
      <div className="message-box">
        <h3 className="message-user-name">{message.user}</h3>
        <p className="message-text">
          {message.text}
        </p>
        <span className="message-time">
          {message.editedAt ? 'изменено  ' + message.editedAt.slice(11, 16) : message.createdAt.slice(11, 16)}</span>
        <span className="message-like"></span>
      </div>
    </div>
  );
};

export default Message;