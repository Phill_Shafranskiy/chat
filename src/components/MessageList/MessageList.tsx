import React, { FC } from 'react';
import './MessageList.css';
import OwnMessage from './OwnMessage/OwnMessage';
import Message from './Message/Message';
import { MessageItem } from '../../types/types';

interface MessageListProps {
  messages: MessageItem[];
}

const MessageList: FC<MessageListProps> = ({ messages }) => {
  return (
    <div className='message-list'>
      <div className="messages-divider">Yesterday</div>
      <OwnMessage />
      {messages.map(message =>
        <Message message={message} />
      )}
    </div>
  );
};

export default MessageList;
