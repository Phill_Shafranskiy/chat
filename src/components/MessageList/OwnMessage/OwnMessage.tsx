import React, { ChangeEventHandler, FC, MouseEventHandler } from 'react';
import './OwnMessage.css';

const handleRemoveItem: MouseEventHandler<HTMLButtonElement> = (event) => {
  console.log(event.target);

  // event.target.closest('.own-message').remove();
}

const OwnMessage: FC = () => {
  return (
    <div className='own-message'>
      <p className="message-text">
        I don’t *** understand. It's the Panama accounts
      </p>
      <span className="message-time">12:24</span>
      <div className="message-buttons">
        <span className="message-edit"></span>
        <button className="message-delete" onClick={handleRemoveItem}></button>
      </div>
    </div>
  );
};

export default OwnMessage;