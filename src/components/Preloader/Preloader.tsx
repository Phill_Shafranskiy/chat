import React, { FC } from 'react';
import './Preloader.css'

const Preloader: FC = () => {
  return (
    <div className="preloader center">
      <i className="fa fa-cog fa-spin" />
    </div>
  )
};

export default Preloader;
